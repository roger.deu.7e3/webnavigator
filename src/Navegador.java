import java.util.*;

/**
 * Classe navegador que implementa totes les funcions que necessitem, treballa amb dues Piles una que emmagatzema urls on he estat per anar enrerre o per anar endevant
 * també conte un arraylist, per emmagaztemar l'historial de pagines web on he estat i un HashMap per emmagatzemar les webs on he estat sense repeticions amb la quantitat de vegades
 * que han sigut visitades.
 *
 * @author Roger Deu Martí
 * @version 07/11/2020
 */

public class Navegador {

    Pila pilaEndevant = new Pila();
    Pila pilaEnrere = new Pila();
    List historial = new ArrayList();
    HashMap<String, Integer> map = new HashMap<>();

    private String url;

    /**
     * Constructor de la classe Navegador
     * @param url rep la primera url
     */
    public Navegador(String url) {
        this.url = url;
    }

    /**
     * Metode per a retornar l'url emmagatzemada en la variable per així poder accedir desde la classe InternetExplorer
     * @return retorna l'url de la web activa
     */
    public String getUrl() {
        return url;
    }

    /**
     * Metode per a assignar una nova url
     * @param url emmagatzema a la variable de la classe
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Aquest metode s'utilitza per anar a una nova pagina web, primer es fa una comprovació de la url per a veure si es valida o no, i despres es passa a la pila per anar enrerre,
     * es guarda en l'historial, s'elimina la pila per anar endevant, ja que hem començat altre vegada les webs que tenim davant, finalment comprovem que la nova url esta en el mapa o no,
     * en cas que no ho estigui l'introduim per primera vegada i en cas que hi sigui , sumem 1 a les aparicions.
     * @param novaURL aquesta es l'url amb la que treballem, i es la nova url a la qual volem anar.
     */
    public void anarA(String novaURL) {
        String regex = "(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]+\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]+\\.[^\\s]{2,})";
        if (novaURL.matches(regex)) {
            pilaEnrere.push(novaURL);
            historial.add(novaURL);
            pilaEndevant.removeAllElements();

            if (map.containsKey(novaURL)) {
                comprobarMapRepetit(novaURL);
            } else {
                map.put(novaURL,1);
            }
            setUrl(novaURL);
            System.out.println("Ara estas a: "+novaURL);
        } else {
            System.out.println("Aquesta URL no es valida");
        }
    }

    /**
     * Amb aquest metode podem visitar la pagina web o les pagines web que hem visitat anteriorment, primer fem un pop de la pilaenrere per així recuperar l'url,
     * l'url que recuperem la guardem en la pila endevant per així poder visitar-la después si volem, i la nova url a utilitzar sera la que treurem amb un peek ja que amb el pop
     * treiem l'url actual que teniem, després l'afegim a l'historial i comprobem el mapa per a sumarli a les visites realitzades.
     * I amb aquesta excepció en cas que la pila estigui buida ens avisa
     */
    public void enrere() {
        String url = "", urlActual = "";
        try {
            url = (String) pilaEnrere.pop();
            pilaEndevant.push(url);
            urlActual = (String) pilaEnrere.peek();
            historial.add(urlActual);
            comprobarMapRepetit(urlActual);
            System.out.println("Ara estas a: "+urlActual);
            setUrl(urlActual);
        } catch (PilaBuidaException p) {
            p.getMessage();
        }

    }

    /**
     * Aquesta metode ens comprova si la url esta contenida ja en el mapa o no, en cas que ho estigui sumem +1 al valor de la clau que es l'url
     * @param url Es l'url actual amb la que estem treballant
     */
    public void comprobarMapRepetit(String url) {
        if (map.containsKey(url)) {
            int valor = map.get(url);
            map.replace(url,valor,valor+1);
        }
    }

    /**
     * Aquest metode ens serveix per anar endevant, agafem l'url de la pila endevant fent un pop, per tant l'agafem i la borrem, després la passem a la pila enrerre, també
     * l'afegim a l'historial i comprobem el mapa.
     * En l'excepcio el que fem es mirar si hi han valors a la pila o no
     */
    public void endavant() {
        String url="";
        try {
            url = (String) pilaEndevant.pop();
            pilaEnrere.push(url);
            historial.add(url);
            comprobarMapRepetit(url);
            System.out.println("Ara estas a: "+url);
            setUrl(url);
        } catch (PilaBuidaException p) {
            p.getMessage();
        }
    }

    /**
     * Aquest metode ens serveix per a imprimir l'historial amb una lambda que es va imprimint l'array list que utilitzem
     */
    public void veureHistorial() {
        System.out.println("L'historial del navegador es: ");
        historial.forEach(x -> System.out.println(x.toString()));
    }

    /**
     * Aquest es el metode inicial per a poder guardar la primera web que es carrega, la guardem en l'historial i l'afegim en el mapa
     */
    public void inici() {
        setUrl("http://www.itb.cat");
        historial.add(url);
        map.put(url,1);
    }

    /**
     * Amb aquest metode imprimim nots els valors del hashmap per veure l'url i totes les visites que ha tingut cadascuna
     */
    public void veureVisitades() {
        System.out.println(map.entrySet());
    }

}
