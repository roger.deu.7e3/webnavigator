/**
 * Aquesta classe conte l'excepcio que controla si la pila esta buida o no
 * @author Roger Deu Marti
 * @version 07/11/2020
 */
public class PilaBuidaException extends Exception {
    /**
     * Aquest metode printa un missatge
     */
    public PilaBuidaException() {
        System.out.println("Error: La pila esta buida");
    }
}
