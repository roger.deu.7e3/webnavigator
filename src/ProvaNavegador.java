import java.util.Scanner;

/**
 * Aquesta classe s'utilitza com a tester del Navegador amb un menu amb el qual pots interectuar i fer les accions que vulguis
 * @author Roger Deu Martí
 * @version 07/11/2020
 */
public class ProvaNavegador {
    /**
     * Funcio main que inicia el navegador i ens crida al metode menu
     * @param args Aquest parametre no fa res
     */
    public static void main(String[] args) {
        Navegador nav = new Navegador("hola");
        menu(nav);
    }

    /**
     * Aquest metode es un menu per a seleccionar l'opcio que vols realitzar
     * @param nav Ens permet passar l'objecte navegador a totes les funcions que el necessiten
     */
    public static void menu(Navegador nav) {
        int opcio;

        System.out.println("Que vols fer ara? \n[1] Anar a\n[2] Enrere\n[3] Endevant\n[4] Veure historial\n[5] Veure visitades");
        Scanner lector = new Scanner(System.in);
        while (!lector.hasNextInt()) {
            System.out.println("Aixo no es un numero");
            lector.next();
        }
        opcio = lector.nextInt();

        switch (opcio) {
            case 0:
                break;
            case 1:
                System.out.println("Introdueix la pagina web on vols anar");
                lector.nextLine();
                String web = lector.nextLine();
                nav.anarA(web);
                menu(nav);
            case 2:
                nav.enrere();
                menu(nav);
            case 3:
                nav.endavant();
                menu(nav);
            case 4:
                nav.veureHistorial();
                menu(nav);
            case 5:
                nav.veureVisitades();
                menu(nav);
            default:
                System.out.println("Aixo no es una opció valida");
                menu(nav);
        }
    }
}
