/**
 * Aquesta classe s'utilitza per a iniciar el JFrame per tal de poder tindre la pantalla del navegador
 *
 * @author Roger Deu Marti
 * @version 07/11/2020
 */
public class MainExplorer {
    /**
     * Aquest es el main que inicialitza la classe navegador amb la que treballarem i el JavaFX per a carregar la finestra
     * @param args Aquest parametre no fa res
     */
    public static void main(String[] args) {
        Navegador nav = new Navegador(".");
        nav.inici();
        InternetExplorer g = new InternetExplorer();
        g.setVisible(true);
    }
}
