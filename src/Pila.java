import java.util.ArrayDeque;
import java.util.Iterator;

/**
 * Aquesta classe pila l'utilitzem per a poder iniciar les dues piles amb les quals treballarem en el nostre
 * programa, amb una ArrayDeque, aquesta classe implementa iterable per a poder treballar amb la pila.
 * @param <E> Especifica el tipus d'objecte amb el que treballarem
 * @author Roger Deu Marti
 * @version 07/11/2020
 */
public class Pila<E> implements Iterable{
    private ArrayDeque<E> pila = new ArrayDeque<>();

    /**
     * Aquest metode el cridarem per a saber si la pila esta buida i hem de cridar l'excepcio o no
     * @return retorna true o false segons l'estat de la pila
     */
    public boolean empty() {
        if (pila.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Aquest metode ens agafa l'ultim element que tenim en la nostra pila
     * @return retirna l'element que hem agafat de la pila
     * @throws PilaBuidaException En cas que la comprovacio que fem ens digui que la pila esta buida, saltara l'excepcio
     */
    public E peek() throws PilaBuidaException {
        if (empty()) {
            throw new PilaBuidaException();
        } else {
            return pila.peek();
        }
    }

    /**
     * Aquest metode ens serveix per a agafar l'ultim objecte de la pila i a més a més el borrem
     * @return ens retorna l'ultim objecte de la pila
     * @throws PilaBuidaException En cas que la comprovacio que fem ens digui que la pila esta buida, saltara l'excepcio
     */
    public E pop() throws PilaBuidaException {
        if (empty()) {
            throw new PilaBuidaException();
        } else {
            return pila.pop();
        }
    }

    /**
     * Aquest metode ens afegeix un altre element a la nostre pila
     * @param e rep un element que en el nostre cas sera una url en format string
     */
    public void push (E e) {
        pila.push(e);
    }

    /**
     * Aquest metode serveix per a borrar el contingut de la pila i deixar-la en el seu estat inicial
     */
    public void removeAllElements() {
        pila.clear();
    }

    /**
     * Metode de la classe que implementem (Iterable)
     * @return retorna l'iterator de la pila
     */
    @Override
    public Iterator iterator() {
        return pila.iterator();
    }
}
